# Promising [![Shippable](https://img.shields.io/shippable/58d4315956628806009ff4b9.svg)]()
An extension to q or Bluebird that adds some cool features.
## Features
*Note: all examples are used in the case of Express/Mongoose, although those
aren't necessary.*
### If Value Is/Else
> Support Status: 1 (Implemented)

```javascript
promising(Model.findOne({})).ifValueIs(null, function() {
  res.status(404);
}).else(function() {
  res.send("It Worked!");
}).endIf(function() {
  // Act as if this is a `then`
  res.end();
});
```
### Switch
> Support Status: 0.5 (Next-To-Do)

```javascript
promising(Model.findOne({})).then((data) => {
  promising
  .case(null, function(data) {
    res.status(410);
    return data;
  })
  .then(function(data) {
    // .case() will affect EVERYTHING after it until either break(), case(),
    // end() or default()
    res.send("Uh oh!");
    return data;
  })
  .case(true, function(data) {
    // Now if it's true then it'll run this
    res.send("Woohoo!");
    return data;
  })
  .default(function(data) {
    // Same as default: ...
    res.send("Yipee!");
    return data;
  })
  .end(function(data) {
    // same as the }
    res.end();
  });
}
```
### Hide Variables except for One
> Support Status: 0 (Not Implemented)

```javascript
// Hide everything but the field password to hashy.hash. We don't want
// Hashy to hash our Model, we want it to hash our password.
promising(Model.findOne({})).hideAllVariablesExceptFor("password", hashy.hash)
  .then(function(data) {
    console.log(data);
    return data;
  })
  .showVariables(function(data) {
    console.log(data[0]); // data[0] is the output from above
    console.log(data[1]); // data[1] is the original data
  });
```
## Installation
```
npm install --save bitbucket:wapidstyle/promising
```
### Usage
Wrap the Promise in promising's function.
```javascript
promise(/* Promise */).then(function(){}).ifValueIs() // and so on
```
