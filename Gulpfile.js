const gulp = require("gulp");
const log = require("fancy-log");

gulp.task("default", () => {
  log("--------------------[Promising]--------------------");
  log("gulp lint                 Run ESLint");
  log("     doc                  Run JSDoc");
  log("     test                 Run test suites with Mocha");
  log("     coverage             Run Istanbul for code coverage");
  log("");
  log("gulp all                  Run EVERYTHING!");
  log("---------------------------------------------------");
});

gulp.task("help", ["default"], () => {});

gulp.task("lint", () => {
  const eslint = require("gulp-eslint");
  return gulp.src("./index.js")
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task("doc", () => {
  const jsdoc = require("gulp-jsdoc3");
  return gulp.src(["README.md", "index.js"], {read: false})
    .pipe(jsdoc());
});

gulp.task("pre-coverage", () => {
  const istanbul = require("gulp-istanbul");
  return gulp.src("./index.js")
    .pipe(istanbul())
    .pipe(istanbul.hookRequire());
});

gulp.task("test", () => {
  const mocha = require("gulp-mocha");
  // XXX(wapidstyle) gulp-mocha outputs 3 times
  return gulp.src("./test/index.test.js", {read: false})
    .pipe(mocha());
});

gulp.task("citest", () => {
  const mocha = require("gulp-mocha");

  return gulp.src("./test/index.test.js", {read: false})
    .pipe(mocha({reporter: "xunit-file"}));
});

gulp.task("coverage", ["pre-coverage"], () => {
  const istanbul = require("gulp-istanbul");
  const mocha = require("gulp-mocha");
  const plumber = require("gulp-plumber");

  return gulp.src("./test/*.js")
    .pipe(plumber())
    .pipe(mocha())
    .pipe(istanbul.writeReports())
    .pipe(istanbul.enforceThresholds({thresholds:{global: 90}}));
});
