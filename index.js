/*
 * Promising library
 * Meant to be an expansion to Promises.
 *
 * This code is under the BSD-3 License.
 */
// TODO Use ES6
/**
  * Turns an ordinary q, Bluebird or ES6 Promise into a Promising one that
  * has bonus methods.
  *
  * @param {Promise} promise The promise to turn into a Promising one.
  * @returns {Promise} The improved Promise.
  */
module.exports = function promising(promise) {
  /**
    * Tracks if it is in an If statement.
    *
    * @type {Number}
    * @private
    */
  promise._inIf = 0;

  /**
    * Tracks the value of the current If statement.
    *
    * @type {Number[]}
    * @private
    */
  promise._ifStatus = Array([]);

  /**
    * Tracks which "ring" the switch is in.
    *
    * @type {Number}
    * @private
    */
  promise._inSwitch = -1;

  /**
    * Tracks which field to check for a switch clause.
    *
    * @type {String[]}
    * @private
    */
  promise._switchField = Array([]);

  /**
    * Tracks weather or not to use the "default" switch gate.
    *
    * @type {Boolean[]}
    * @private
    */
  promise._runDefault = Array([]);

  /**
    * Small behind-the-scenes function to keep a good copy of Promise.then()
    *
    * @param {Function} callback The function to call
    * @returns {Promise} The promise
    * @memberof {Promise}
    * @private
    */
  promise._then = promise.then.bind(promise);

  /**
    * A modified version of .then that also returns a compatable Promise.
    *
    * @param {Function} callback The function to call
    * @returns {Promise} The promise
    * @memberof {Promise}
    */
  promise.then = function then(callback) {
    this._then(callback);
    return this;
  };

  /**
    * Checks if the value in the Promise is the specified value.
    *
    * @param {Object} value The object to check.
    * @param {String} [field] The field to check.
    * @param {Function} callback The function to call if true.
    *
    * @returns {Promise} The Promise
    * @memberof {Promise}
    */
  promise.ifValueIs = function ifValueIs(value, field, callback) {
    if(typeof callback === "undefined" && typeof field === "function") {
      callback = field;
    }
    this.then(function _ifValueIs(data) {
      if(typeof this._inIf === "undefined") {
        this._inIf = 0;
      }
      if(typeof this._ifStatus === "undefined") {
        this._ifStatus = [];
      }
      this._inIf += 1;
      if(field === callback) {
        this._ifStatus[this._inIf] = (data === value);
        if(data === value) {
          callback(data);
        }
      } else {
        // Field was selected
        if(typeof data[field] === "undefined") {
          this._inIf -= 1;
          this._ifStatus[this._inIf + 1] = undefined;
          this.ifValueIs(value, callback);
        }
        this._ifStatus[this._inIf] = (data[field] === value);

        if(data[field] === value) {
          callback(data);
        }
      }
      return this;
    });
    return this;
  };

  /**
    * If the value from ifValueIs is false, then this runs.
    *
    * @param {Function} callback The function to run if needed.
    *
    * @returns {Promise} The Promise
    * @memberof {Promise}
    */
  promise.else = function elseThen(callback) {
    this.then(function _elseThen(data) {
      if(this._ifStatus[this._inIf] === false) {
        callback(data);
        return this;
      } else {
        return this;
      }
    });
    return this;
  };

  /**
    * Ends an ifValueIs statement. Acts as a .then() in the callback.
    *
    * @param {Function} callback The callback
    * @returns {Promise} The promise
    * @memberof {Promise}
    */
  promise.endIf = function endIf(callback) {
    this.then(function _endIf(data) {
      this._inIf -= 1;
      this._ifStatus[this._inIf + 1] = undefined;
      return data;
    }).then(callback);

    return this;
  };

  /**
    * Starts a switch statement. Can start on specific field.
    *
    * @param {String} [field] The field that the statement should operate on.
    * @returns {Promise} The promise
    * @memberof {Promise}
    */
  promise.switch = function switchStatement(field) {
    this.then(function _switchStatement(data) {
      if(typeof this._inSwitch !== "undefined" &&
        typeof this._runDefault !== "undefined" &&
        typeof this._switchField !== "undefined") {
        this._inSwitch += 1;
        this._runDefault = true;
      } else {
        this._inSwitch = 0;
        this._runDefault = true;
        this._switchField = [];
      }

      if(field) {
        this._switchField.push(field);
      } else {
        this._switchField.push(null);
      }

      return data;
    });

    return this;
  };

  /**
    * "Case" statement for the Switch feature.
    *
    * @param {Object} comparative What to compare the to
    * @param {Function} callback A function to execute as the callback.
    * @returns {Promise} The promise
    * @memberof {Promise}
    */
  promise.case = function switchCase(comparative, callback) {
    this.then(function _switchCase(data) {
      if(this._switchField[this._inSwitch] === null) {
        this.ifValueIs(comparative, function _switchCaseIf(nData) {
          this._runDefault = false;
          callback(nData);
        });
      } else {
        this.ifValueIs(comparative, this._switchField, function _switchCaseIf(nData) {
          callback(nData);
        });
      }

      return data;
    });

    return this;
  };

  /**
    * "Default" statement for the Switch feature - activates if all statements
    * above it fail.
    *
    * @param {Function} callback The callback to run
    * @returns {Promise} The promise
    * @memberof {Promise}
    */
  promise.default = function switchDefault(callback) {
    this.then(function _switchDefault(data) {
      if(this._runDefault) {
        callback(data);
      }

      return data;
    });

    return this;
  };

  /**
    * Ends a switch element.
    *
    * @param {Function} callback The callback to run
    * @returns {Promise} The promise
    * @memberof {Promise}
    */
  promise.endSwitch = function endSwitch(callback) {
    this.then(function _endSwitch(data) {
      this._inSwitch -= 1;
      this._switchField[this._inSwitch + 1] = undefined;

      callback(data);
      return data;
    });

    return this;
  };
  return promise;
};
