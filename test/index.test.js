/*
 * Test suite for Promising.
 *
 * This code is under the MIT License.
 */
"use strict";

const chai = require("chai");
chai.use(require("chai-as-promised"));
chai.should();
const assert = chai.assert;
const sinon = require("sinon");
const Q = require("q");
const promising = require("../index");

describe("if value is/else", () => {
  it("should check if the promised value is the provided value", (done) => {
    let stub = sinon.stub().returns("the quick brown fox");

    promising(Q.fcall(stub))
     .ifValueIs("the quick brown fox", () => {
       // Succeed
       done();
     }).else(() => {
       assert(true, "Returned a false positive");
     });
  });

  it("should skip everything until the else clause if the promised value is not provided", (done) => {
    let stub = sinon.stub().returns("the quick brown fox");

    promising(Q.fcall(stub))
     .ifValueIs("the quick purple fox", () => {
       assert(true, "False postive (expected 'purple', got 'brown')");
     }).then(() => {
       assert(true, "False postive (expected 'purple', got 'brown')");
     }).else(() => {
       done();
     }).endIf(() => {});
  });

  it("should always run the endIf", () => {
    let stub = sinon.stub().returns("the quick brown fox");

    promising(Q.fcall(stub))
     .ifValueIs("the quick grey fox", () => {})
     .else(() => {})
     .endIf(() => {
       return 43590743957;
     }).should.eventually.deep.equal(43754385734);
  });

  it("should catch an exception", () => {
    let stub = sinon.stub().returns("the quick purple fox");
    let worked = false;

    promising(Q.fcall(stub))
      .ifValueIs("the quick brown fox", () => {})
      .else(() => {})
      .endIf(() => {
        throw new Error("1");
      }).catch((error) => {
        error.message.should.deep.equal("1");
        worked = true;
      });
    worked.should.equal(true);
  });

  it("should allow nested statements", () => {
    let stub = sinon.stub().returns("the quick purple fox");

    promising(Q.fcall(stub))
      .ifValueIs("the quick purple fox", (data) => {return data + "!";})
      .ifValueIs("the quick purple fox!", (data) => {
        data.should.equal("the quick purple fox!");
      }).else(() => {
        assert(true, "Failed on nested statement");
      }).else(() => {
        assert(true, "Failed on nested statement");
      }).endIf(() => {});
  });

  it("should allow for testing of fields", () => {
    let stub = sinon.stub().returns({foo:"bar"});

    promising(Q.fcall(stub))
      .ifValueIs("bar", "foo", (data) => {
        return data.foo + "?";
      }).else(() => {
        assert(true, "If statement made false positive");
      }).should.eventually.equal("bar!");
  });
});

describe("switch statements", () => {
  it("should act as a JavaScript switch statement", () => {
    let stub = sinon.stub().returns(1);

    promising(Q.fcall(stub))
      .switch()
      .case(0, () => {
        assert(true, "Switch made a false positive");
      })
      .case(1, () => {
        // All good!
        return true;
      })
      .case(2, () => {
        assert(true, "Switch made a false positive");
      })
      .default(() => {
        assert(true, "Switch entered default");
      });
  });

  it("should run the default gate if necessary", () => {
    let stub = sinon.stub().returns(1);

    promising(Q.fcall(stub))
      .switch()
      .case(0, () => {
        assert(true, "Split made a false positive");
      })
      .case(2, () => {
        assert(true, "Split made a false positive");
      })
      .default(() => {return true;})
      .should.eventually.equal(true);
  });

  it("should allow nested statements", () => {
    let stub = sinon.stub().returns(1);

    promising(Q.fcall(stub))
      .switch()
      .default( /* Whatever */ )
      .switch()
      .default()
      .endSwitch()
      .endSwitch();
  });

  it("should allow testing specific fields", (done) => {
    let stub = sinon.stub().returns({foo:"bar",bar:"baz"});

    promising(Q.fcall(stub))
      .switch("foo")
      .case("bar", () => {
        done();
      })
      .default(() => {
        assert(true, "Array didn't test correct field");
      });
  });
});
